const express = require("express")
const {getAllBoards ,
    postBoard,
    getLists,
    postList,
    deleteList,
    getCards,
    postCard,
    deleteCard,
    getChecklist,
    postChecklist,
    deleteChecklist,
    getCheckItem,
    postCheckItem,
    checkItemCheckBox,
    deleteCheckItem


} = require('./../controller/trelloController')

const router = express.Router()

router.route("/")
.get(getAllBoards)
.post(postBoard)

router.route('/list/:id')
.get(getLists)
.post(postList)
.delete(deleteList)

router.route('/card/:id')
.get(getCards)
.post(postCard)
.delete(deleteCard)

router.route('/checklist/:id') 
.get(getChecklist)
.post(postChecklist)
.delete(deleteChecklist)

router.route('/checkItem/:id')
.get(getCheckItem )
.post(postCheckItem)
.delete(deleteCheckItem)
.put(checkItemCheckBox)


module.exports = router