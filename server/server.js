const express = require('express')
var cors = require('cors')
const trello = require('./routes/trelloRoute')

const app = express()

const port = 4000

app.use(express.json())
app.use(cors())

app.use('/', trello)

app.listen(port, () => {
    console.log(`app is listening at port ${port}`)
})