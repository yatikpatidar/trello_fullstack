const db = require('./../models')

const Board = db.boards
const List = db.lists
const Card = db.cards
const Checklist = db.checklists
const CheckItem = db.checkitems


const getAllBoards = async (req, res) => {
    try {
        const response = await Board.findAll({ attributes: ['id', 'board_name'] })
        res.status(200).json(response);
    } catch (error) {
        res.status(500).json({ error: 'Internal server error' });
    }
}

const postBoard = async (req, res) => {

    const info = {
        board_name: req.query.name
    }
    try {
        const response = await Board.create(info);
        res.status(201).json(response);
    } catch (error) {
        res.status(500).json({ error: 'Internal server error' });
    }
}

const getLists = async (req, res) => {
    const id = req.params.id
    try {
        const response = await List.findAll({ where: { board_id: id } }, { attributes: ['id', 'list_name'] });
        res.status(200).json(response);
    } catch (error) {
        res.status(500).json({ error: 'Internal server error' });
    }
}

const postList = async (req, res) => {

    const info = {
        list_name: req.query.name,
        board_id: parseInt(req.params.id)
    }
    try {
        const response = await List.create(info);
        res.status(201).json(response);
    } catch (error) {
        res.status(500).json({ error: 'Internal server error' });
    }
}


const deleteList = async (req, res) => {
    const id = req.params.id
    try {
        const response = await List.destroy({ where: { id:req.params.id } })
        res.status(200).json(response)

    } catch (error) {
        res.status(500).json({ error: 'Internal server Error' })

    }
}


const getCards = async (req, res) => {
    try {
        const response = await Card.findAll({ where: { list_id: req.params.id} }, { attributes: ['id', 'card_name'] });
        res.status(200).json(response);
    } catch (error) {
        res.status(500).json({ error: 'Internal server error' });
    }
}

const postCard = async (req, res) => {
    const info = {
        card_name: req.query.name,
        list_id: parseInt(req.params.id)
    }

    try {
        const response = await Card.create(info);
        res.status(201).json(response)

    } catch (error) {
        res.status(500).json({ error: 'Internal server error' });

    }
}

const deleteCard = async (req, res) => {
    try {
        const response = await Card.destroy({ where: { id:req.params.id } })
        res.status(200).json(response);

    } catch (error) {
        res.status(500).json({ error: 'Internal server Error' })

    }
}


const getChecklist = async (req, res) => {
    try {
        const response = await Checklist.findAll({ where: { card_id: req.params.id} }, { attributes: ['id', 'checklist_name'] });
        res.status(200).json(response);

    } catch (error) {
        res.status(500).json({ error: 'Internal server error' });

    }
}

const postChecklist = async (req, res) => {
    const info = {
        checklist_name: req.query.name,
        card_id: parseInt(req.params.id)
    }

    try {
        const response = await Checklist.create(info);
        res.status(201).json(response)

    } catch (error) {
        res.status(500).json({ error: 'Internal server error' });

    }
}

const deleteChecklist = async (req, res) => {
    try {
        const response = await Checklist.destroy({ where: { id:req.params.id } })
        res.status(200).json(response)

    } catch (error) {
        res.status(500).json({ error: 'Internal server error' })

    }
}


const getCheckItem = async (req, res) => {
    try {
        const response = await CheckItem.findAll({ where: { checklist_id: req.params.id} }, { attributes: ['id', 'checkItem_name' , 'state'] });
        res.status(200).json(response)

    } catch (error) {
        res.status(500).json({ error: 'Internal server Error' })

    }
}

const postCheckItem = async (req, res) => {
    const info = {
        checkItem_name: req.query.name,
        state : req.query.state === 'complete'? 'complete':'incomplete',
        checklist_id: parseInt(req.params.id)
    }
    try {
        const response = await CheckItem.create(info);
        
        res.status(201).json(response)
    } catch (error) {
        res.status(500).json({ error: 'Internal Server Error' })

    }
}

const checkItemCheckBox = async (req, res) => {
    try {
        const response = await CheckItem.update(req.query , {where :{id : req.params.id}})
        res.status(201).json(response)
    } catch (error) {
        res.status(500).json({ error: 'Internal Server Error' })

    }
}

const deleteCheckItem = async (req, res) => {
    try {
        const response = await CheckItem.destroy({ where: { id:req.params.id } })
        res.status(200).json(response)

    } catch (error) {
        res.status(500).json({ error: 'Internal server Error' })

    }
}


module.exports = {
    getAllBoards,
    postBoard,
    getLists,
    postList,
    deleteList,
    getCards,
    postCard,
    deleteCard,
    getChecklist,
    postChecklist,
    deleteChecklist,
    getCheckItem,
    postCheckItem,
    checkItemCheckBox,
    deleteCheckItem
}