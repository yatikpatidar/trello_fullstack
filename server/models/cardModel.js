
module.exports = (sequelize, DataTypes) => {
    const Card = sequelize.define("cards", {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        card_name: {
            type: DataTypes.TEXT,
            allowNull: false
        },
        list_id: {
            type: DataTypes.INTEGER,
        }
    })

    return Card
}