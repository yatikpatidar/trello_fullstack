

module.exports = (sequelize, DataTypes) => {
    console.log("inside boards")
    const Board = sequelize.define("boards", {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        board_name: {
            type: DataTypes.TEXT,
            allowNull: false
        }
    })

    return Board
}