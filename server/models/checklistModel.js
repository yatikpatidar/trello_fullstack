
module.exports = (sequelize, DataTypes) => {
    const Checklist = sequelize.define("checklist", {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        checklist_name: {
            type: DataTypes.TEXT,
            allowNull: false
        },
        card_id: {
            type: DataTypes.INTEGER,
        }
    })

    return Checklist
}