

module.exports = (sequelize, DataTypes) => {

    const List = sequelize.define("lists", {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        list_name: {
            type: DataTypes.TEXT,
            allowNull: false
        },
        board_id :{
            type: DataTypes.INTEGER,
        }
    })

    return List
}