module.exports = (sequelize, DataTypes) => {
    const Checklist = sequelize.define("checkItems", {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        checkItem_name: {
            type: DataTypes.TEXT,
            allowNull: false
        },
        state:{
            type: DataTypes.TEXT,
            defaultValue: 'incomplete'
        },
        checklist_id: {
            type: DataTypes.INTEGER,
        }
    })

    return Checklist
}