const sequelize = require('./../config/dbConfig')

const { Sequelize, DataTypes } = require('sequelize');

sequelize.authenticate()
    .then(() => {
        console.log("we are connected to database ! ")
    }).catch((err) => {
        console.log("something went wrong while connecting to database !", err)
    })

const db = {}

db.Sequelize = Sequelize
db.sequelize = sequelize

db.boards = require('./../models/boardModel.js')(sequelize, DataTypes)
db.lists = require('./../models/listModel.js')(sequelize, DataTypes)
db.cards = require('./../models/cardModel.js')(sequelize, DataTypes)
db.checklists = require('./../models/checklistModel.js')(sequelize, DataTypes)
db.checkitems = require('./../models/checkItemModel.js')(sequelize, DataTypes)

db.sequelize.sync({ force: false })
    .then(() => {
        console.log('yes re-sync Done !')
    })

module.exports = db