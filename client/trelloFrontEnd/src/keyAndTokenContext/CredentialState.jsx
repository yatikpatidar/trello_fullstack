import React from "react";
import CredentialContext from "./CredentialContext";


export default function CredentialState(props) {

  const user = {
    'key': '7bccfa9b4b0c0f95210841692d91818c',
    'token': 'ATTA28b56d395c0bed67cf2b81965c9c10d98bf7861b554d2a1559b5497ac089b87b4CBBFE02'
  }
  return (
    <>
      <CredentialContext.Provider value={{ user }}>
        {props.children}

      </CredentialContext.Provider>

    </>
  )
}

