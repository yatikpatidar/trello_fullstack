import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    boards: []
}

export const boardsSlice = createSlice({
    name: 'boards',
    initialState,
    reducers: {
        fetchBoards: (state, action) => {
            state.boards = action.payload
        },

        addBoards: (state, action) => {
            state.boards = [action.payload, ...state.boards]
        }   
    }

})

export const { fetchBoards, addBoards } = boardsSlice.actions

export default boardsSlice.reducer