import { createSlice } from "@reduxjs/toolkit";
import { act } from "react";

const initialState = {
    checkItem :{}
}

export const checkItemSlice = createSlice({
    name:'checkItem',
    initialState,
    reducers:{
        setCheckItem : (state , action)=>{
            state.checkItem[action.payload.checklistId] = action.payload.jsonData
        },

        addCheckItem:(state , action) =>{
            if(state.checkItem[action.payload.checklistId] === undefined){
                state.checkItem[action.payload.checklistId] = [action.payload.response]
            }else{
                state.checkItem[action.payload.checklistId] = [action.payload.response , ...state.checkItem[action.payload.checklistId]]

            }
        },

        deleteCheckItem : (state , action)=>{
            state.checkItem[action.payload.checklistId] = state.checkItem[action.payload.checklistId].filter((checkItem) => checkItem['id'] !== action.payload.checkItemId)
        },

        updateCheckItem:(state , action)=>{

            state.checkItem[action.payload.checklistId] = state.checkItem[action.payload.checklistId].map((checkItem)=>{
                if(checkItem['id'] === action.payload.checkItemId){
                    return {
                        ...checkItem , state : action.payload.state
                    }

                }else{
                    return checkItem
                }
            })
        }
    }
})

export const {setCheckItem ,addCheckItem , deleteCheckItem , updateCheckItem} = checkItemSlice.actions
export default checkItemSlice.reducer