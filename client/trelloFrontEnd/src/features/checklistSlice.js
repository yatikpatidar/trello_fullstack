import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    checklistData: [],
}

export const checklistSlice = createSlice({
    name: 'checklist',
    initialState,
    reducers: {
        setChecklist: (state, action) => {
            state.checklistData = action.payload;
        },

        addChecklist: (state, action) => {
            if (state.checklistData.length === 0) {
                state.checklistData = [action.payload]
            } else {
                state.checklistData = [action.payload , ...state.checklistData]
            }
        },

        deleteChecklist: (state, action) => {
            state.checklistData = state.checklistData.filter((checklist) => checklist['id'] !== action.payload)
        },

        addCheckItem: (state, action) => {
            state.checklistData = state.checklistData.map((checklist) => {
                if (checklist['id'] === action.payload.checklistId) {
                    return {
                        ...checklist,
                        checkItems: [...checklist['checkItems'], action.payload.response]
                    }

                } else {
                    return checklist
                }
            })
        },

        updateCheckItem: (state, action) => {
            state.checklistData = state.checklistData.map((checklist) => {
                if (checklist['id'] === action.payload.checklistId) {
                    return {
                        ...checklist,
                        checkItems:
                            checklist['checkItems'].map((checkItem) => {
                                if (checkItem['id'] === action.payload.checkItemId) {
                                    return action.payload.response
                                } else {
                                    return checkItem
                                }
                            })
                    }
                } else {
                    return checklist
                }
            })
        },

        deleteCheckItem:(state , action)=>{
            state.checklistData = state.checklistData.map((checklist) => {
                if (checklist['id'] === action.payload.checklistId) {
                    return {
                        ...checklist,
                        checkItems:
                            checklist['checkItems']
                                .filter((checkItem) => checkItem['id'] !== action.payload.checkItemId)
                    }
                } else {
                    return checklist
                }
            })
        }
    }

})

export const { setChecklist, addChecklist, deleteChecklist} = checklistSlice.actions
export default checklistSlice.reducer