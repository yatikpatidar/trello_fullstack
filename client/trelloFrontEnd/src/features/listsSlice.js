import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    listsData: []
}

export const listsSlice = createSlice({
    name: 'lists',
    initialState,
    reducers: {
        fetchLists: (state, action) => {
            state.listsData = action.payload
        },

        addList: (state, action) => {
            if (state.listsData === undefined) {
                state.listsData = [action.payload]
            } else {
                state.listsData = [action.payload, ...state.listsData]
            }
        },

        deleteList: (state, action) => {
            state.listsData = state.listsData.filter((list) => list['id'] !== action.payload)
        }
    }
})

export const { fetchLists, addList, deleteList } = listsSlice.actions
export default listsSlice.reducer