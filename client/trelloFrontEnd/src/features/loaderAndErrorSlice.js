import { createSlice } from "@reduxjs/toolkit";

const initialState ={
    loader : false,
    error : ""
}

export const loaderAndErrorSlice = createSlice ({
    name:'loaderAndError',
    initialState,
    reducers:{
        setLoader:(state , action)=>{
            state.loader = action.payload
        } ,

        setError:(state , action)=>{
            state.error = action.payload
        }

    }
})

export const {setLoader , setError} = loaderAndErrorSlice.actions

export default loaderAndErrorSlice.reducer