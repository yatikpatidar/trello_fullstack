import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    cardsData: {}
}

export const cardsSlice = createSlice({
    name: 'cards',
    initialState,
    reducers: {
        setCards: (state, action) => {
            state.cardsData[action.payload.listId] = action.payload.jsonData
        },

        addCard: (state, action) => {
            if (state.cardsData[action.payload.listId] === undefined) {
                state.cardsData[action.payload.listId] = [action.payload.response]
            } else {
                state.cardsData[action.payload.listId] = [action.payload.response , ...state.cardsData[action.payload.listId]]
            }
        },

        deleteCard: (state, action) => {
            state.cardsData[action.payload.listId] = state.cardsData[action.payload.listId].filter((card) => card['id'] !== action.payload.cardId)
        }
    }
})

export const { setCards, addCard, deleteCard } = cardsSlice.actions

export default cardsSlice.reducer