import axios from "axios";
import { useSelector, useDispatch } from "react-redux";
import { setLoader, setError } from "../features/loaderAndErrorSlice";


export const trelloApi = (fetchUrl, addUrl, deleteUrl) => {

    const dispatch = useDispatch()

    const getData = async () => {

        try {
            dispatch(setError(``))
            dispatch(setLoader(true))
            let jsonData = await axios.get(fetchUrl);

            jsonData = jsonData['data']
            return jsonData

        } catch (err) {
            dispatch(setError(`${err.message}`))
        } finally {
            dispatch(setLoader(false))
        }
    }

    const postData = async (value) => {
        try {
            dispatch(setLoader(true))
            addUrl = addUrl.replace('{name}', value)
            const response = await axios.post(addUrl)

            return response.data

        } catch (err) {
            dispatch(setError(`${err.message} , Reason :- ${err}`))
        } finally {
            dispatch(setLoader(false))
        }
    }

    const deleteData = async (id, method) => {
        try {
            dispatch(setLoader(true))

            deleteUrl = deleteUrl.replace('{deleteId}', id)
            const response = await axios({
                url: deleteUrl,
                method: method.toLowerCase()
            })

        } catch (err) {
            dispatch(setError(`${err.message} , Reason :- ${err.response.data}`))
        } finally {
            dispatch(setLoader(false))
        }
    }


    const putData = async (url) => {
        try {
            dispatch(setLoader(true))
            const response = await axios.put(url)
            return response.data

        } catch (err) {
            dispatch(setError(`${err.message} , Reason :- ${err.response.data}`))
        } finally {
            dispatch(setLoader(false))
        }
    }

    return [getData, postData, deleteData, putData]
}
