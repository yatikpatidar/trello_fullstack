import React from 'react'
import { Outlet } from 'react-router-dom'
import Navbar from './components/common/Navbar'
import CredentialState from './keyAndTokenContext/CredentialState'

const Layout = () => {
  return (
    <>
      <CredentialState>

        <Navbar />
        <Outlet />
      </CredentialState>
    </>
  )
}

export default Layout