import React, { useState, useEffect, useContext, useReducer } from 'react'
import DisplayBoard from '../components/DisplayBoard';
import CreateBoard from '../components/common/AddModal';
import ErrorHandling from '../components/common/ToastError';

import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Loader from '../components/common/Loader';

import { trelloApi } from '../api/trelloApi';
import { useSelector, useDispatch } from 'react-redux'
import { fetchBoards, addBoards } from '../features/boardSlice';

export default function Home() {

    const fetchUrl = `http://localhost:4000/`
    const addUrl = `http://localhost:4000/?name={name}`
    const deleteUrl = ``

    const [
        getData,
        postData
    ] = trelloApi(fetchUrl, addUrl, deleteUrl);

    const boardsData = useSelector((state) => state.boards.boards)
    const loading = useSelector((state) => state.loaderAndError.loader)
    const error = useSelector((state) => state.loaderAndError.error)
    const dispatch = useDispatch()

    useEffect(() => {
        async function fetchData() {
            const jsonData = await getData()
            dispatch(fetchBoards(jsonData))
        }

        fetchData()
    }, [])

    async function handleAddBoard(value) {
        const response = await postData(value)
        dispatch(addBoards(response))
    }


    return (
        <>
            {error && <ErrorHandling error={error} />}

            {loading && <Loader loader={loading} />}

            <Box sx={boardAndcreateStyle}>
                <Typography variant="h5" sx={{ p: 1, px: 4 }} > Boards </Typography>

                <CreateBoard onSubmitModal={handleAddBoard} label={"Board"} />
            </Box>

            <Grid px={5} my={4} container spacing={5} >
                {boardsData && boardsData.map((board, index) => {
                    return (
                        <Grid key={`boards${index}`} item lg={4} sx={{ textAlign: 'center' }} >
                            <DisplayBoard board={board} />
                        </Grid>
                    )
                })}
            </Grid>

            
        </>
    )
}

const boardAndcreateStyle = {
    display: 'flex',
    justifyContent: 'space-between',
    shadows: '20px',
    width: '60%',
    marginLeft: '20%',
    p: 2,
    borderRadius: 2,
    borderBottom: '2px solid gray',

}