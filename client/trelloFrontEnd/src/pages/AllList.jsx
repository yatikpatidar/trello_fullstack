import React, { useEffect, useState, useContext, useReducer } from 'react'
import { useParams, Link } from 'react-router-dom'
import DisplayList from '../components/DisplayList';
import CreateList from '../components/common/AddModal';
import ErrorHandling from '../components/common/ToastError';
import CredentialContext from '../keyAndTokenContext/CredentialContext';
import Loader from '../components/common/Loader';

import Divider from '@mui/material/Divider';
import Stack from '@mui/material/Stack';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';


import { trelloApi } from '../api/trelloApi';
import { useSelector, useDispatch } from 'react-redux'
import { fetchLists, addList, deleteList } from '../features/listsSlice';

export default function AllList() {

    const { id } = useParams()
    const { user } = useContext(CredentialContext)

    const fetchUrl = `http://localhost:4000/list/${id}`
    const addUrl = `http://localhost:4000/list/${id}?name={name}`
    const deleteUrl = `http://localhost:4000/list/{deleteId}`

    const [
        getData,
        postData,
        deleteData
    ] = trelloApi(fetchUrl, addUrl, deleteUrl, false);

    const listsData = useSelector((state) => state.lists.listsData)
    const loading = useSelector((state) => state.loaderAndError.loader)
    const error = useSelector((state) => state.loaderAndError.error)
    const dispatch = useDispatch()


    useEffect(() => {

        async function fetchListsData() {
            const jsonData = await getData()
            dispatch(fetchLists(jsonData))
        }

        fetchListsData()
    }, [])

    async function handleNewList(listName) {

        const response = await postData(listName)
        dispatch(addList(response))
    }

    async function handleDeleteList(id) {
        await deleteData(id, 'delete')
        dispatch(deleteList(id))
    }

    return (
        <>
            {error && <ErrorHandling error={error} />}

            <Stack
                direction="column"
                divider={<Divider orientation="vertical" flexItem />}
                spacing={2}
                flexShrink={0}
                flexWrap={'nowrap'}
                position={'relative'}

            >
                <Box component="section" sx={{ p: 2, boxShadow: 3 }}>

                    <CreateList onSubmitModal={handleNewList} label={"List"} user={user} />

                </Box>

                <Box>
                    <Link to={`/`} style={{ textDecoration: 'none' }} >
                        <Button variant="contained" startIcon={<ArrowBackIcon />} sx={{ ml: 3 }} >Back</Button>
                    </Link>

                </Box>

                <Box component="section" sx={{ display: 'flex', width: "100%", p: 2, maxWidth: '95vw', overflowX: 'auto', justifyContent: 'flex-start', alignItems: 'flex-start' }}>
                    {listsData && listsData.map((list, index) => {

                        return <>{list && <DisplayList key={`list${index}`} list={list} onDeleteList={handleDeleteList} />}</>
                    })}

                </Box>

            </Stack>
            {loading && <Loader loader={loading} />}
        </>
    )
}

