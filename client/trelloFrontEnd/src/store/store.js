import { configureStore } from '@reduxjs/toolkit'
import boardSlice from '../features/boardSlice'
import listsSlice from '../features/listsSlice'
import cardsSlice from '../features/cardsSlice'
import checklistSlice from '../features/checklistSlice'
import checkItemSlice from '../features/checkItemSlice'
import loaderAndErrorSlice from '../features/loaderAndErrorSlice'

export const store = configureStore({
  reducer: {
    boards: boardSlice,
    lists: listsSlice,
    cards: cardsSlice,
    checklist : checklistSlice,
    checkItem : checkItemSlice,
    loaderAndError:loaderAndErrorSlice,
  },
})