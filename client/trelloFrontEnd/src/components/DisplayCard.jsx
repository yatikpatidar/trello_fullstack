import React, { useState, useContext, useReducer } from 'react'
import DisplayChecklist from './DisplayChecklist';
import Form from './common/Form';

import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import IconButton from '@mui/material/IconButton';
import Stack from '@mui/material/Stack';
import DeleteIcon from '@mui/icons-material/Delete'
import Button from '@mui/material/Button';
import CloseIcon from '@mui/icons-material/Close';
import Modal from '@mui/material/Modal';

import { trelloApi } from '../api/trelloApi';

import { useSelector, useDispatch } from 'react-redux'
import { setChecklist, addChecklist, deleteChecklist} from '../features/checklistSlice';


const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    borderRadius: 2,
    p: 2,
    height: '80vh'
};

export default function DisplayCard({ card, Item, onDeleteCard }) {

    const fetchUrl = `http://localhost:4000/checklist/${card['id']}`
    const addUrl = `http://localhost:4000/checklist/${card['id']}?name={name}`
    const deleteUrl = `http://localhost:4000/checklist/{deleteId}`

    const [openModal, setOpenModal] = useState(false)
    const [showAddChecklistForm, setShowAddChecklistForm] = useState(false)

    const [
        getData,
        postData,
        deleteData

    ] = trelloApi(fetchUrl, addUrl, deleteUrl);

    const checklistsData = useSelector((state) => state.checklist.checklistData)
    const dispatch = useDispatch()

    function handleClose(e) {
        e.stopPropagation();
        setOpenModal(false)
    };

    function handleDelete(e, id) {
        e.stopPropagation();
        onDeleteCard(id)
    }

    function handleToggleButton(e) {
        e.stopPropagation();
        setShowAddChecklistForm((prev) => !prev)
    }

    async function handleClickOpen(id) {
        setOpenModal(true)
        const jsonData = await getData()
        dispatch(setChecklist(jsonData))
    }

    async function handleAddChecklist(checklistName) {
        const response = await postData(checklistName)
        dispatch(addChecklist(response))
    }

    async function handleDeleteChecklist(e, idChecklist) {
        e.stopPropagation();
        await deleteData(idChecklist, 'delete')
        dispatch(deleteChecklist(idChecklist))
    }


    return (

        <>
            <Box onClick={(e) => handleClickOpen(card['id'])} component="section" spacing={4} sx={{ p: 1, borderRadius: 2 }}>
                <Item sx={{ display: 'flex', justifyContent: 'space-between' }}><Typography gutterBottom variant="h6" component="div" sx={{ textDecoration: 'none' }} >
                    {card['card_name']}
                </Typography>
                    <IconButton onClick={(e) => handleDelete(e, card['id'])} aria-label="delete" sx={{ '&:hover': { color: "#b71c1c" } }}>
                        <DeleteIcon fontSize="small" />
                    </IconButton>
                </Item>
            </Box>
            <Modal
                open={openModal}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style} >
                    <Box sx={{ display: 'flex', justifyContent: 'space-between', ml: 3 }}>
                        <Typography id="modal-modal-title" variant="h4" component="h2">
                            {card['card_name']}
                        </Typography>

                        <Box sx={{ display: 'flex', justifyContent: 'space-between' }} >
                            <Button onClick={(e) => handleToggleButton(e)} variant="" >Add Checklist</Button>
                            <IconButton onClick={(e) => handleClose(e)} sx={{ color: 'black', '&:hover': { color: "#b71c1c" } }}>
                                <CloseIcon />
                            </IconButton>
                        </Box>
                    </Box>

                    <Stack spacing={2} sx={{ mt: 2 }} direction="row">
                        <div style={{ margin: 3, display: 'flex', flexDirection: 'column', width: '100%', maxHeight: '70vh', overflowY: 'auto' }}>

                            {checklistsData.map((checklist, index) => {
                                return <div key={`checklist${index}`}>
                                    <DisplayChecklist
                                        checklist={checklist}
                                        Item={Item}
                                        onDeleteChecklist={handleDeleteChecklist}
                                    />
                                </div>
                            })}

                            {showAddChecklistForm && (
                                <Form onSubmitForm={handleAddChecklist} label={"Checklist"} />
                            )}
                        </div>

                    </Stack>
                </Box>
            </Modal>
        </>

    )
}

