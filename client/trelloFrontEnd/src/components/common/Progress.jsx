import React, { useEffect, useState } from "react";
import Box from "@mui/material/Box";
import LinearProgress from "@mui/material/LinearProgress";
import Typography from '@mui/material/Typography';



export default function Progress({ checkItems }) {
    const [progress, setProgress] = useState(0);
    const totalLength = checkItems.length;

    const checkedLength = checkItems.reduce((result, checkItem) => {
        if (checkItem.state === 'complete') {
            result += 1;
        }
        return result;
    }, 0);

    useEffect(() => {

        if (totalLength === 0) {
            setProgress(0);
        } else {
            const newProgress = (checkedLength / totalLength) * 100
            setProgress(newProgress);
        }
    })

    return (
        totalLength > 0 && (<Box sx={{ display: 'flex', width: '100%' }}>

            <LinearProgress variant="determinate" value={progress} sx={{ width: '90%', mt: 1, mr: 1, h: 9 }} />
            <Box sx={{ width: '10%' }}>
                <Typography variant="body2" color="text.secondary">{`${Math.round(progress)}%`}</Typography>
            </Box>

        </Box>)
    );
};

