import React from 'react'
import Box from '@mui/material/Box';

import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';

const Form = ({ onSubmitForm, label }) => {

    function handleSubmit(e) {

        const inputValue = e.target.inputField.value.trim();
        e.preventDefault()
        if (inputValue.length > 0) {
            onSubmitForm(inputValue)
            e.target.inputField.value = ""
        }
    }

    return (
        <Box component="section" spacing={4} sx={{ display: 'flex', justifyContent: 'center', m: 1, p: 2, boxShadow: 2 }}>

            <form onSubmit={(e) => handleSubmit(e)} sx={{ display: 'flex' }} >

                <TextField id="outlined-basic" label={`+ Add a ${label}`} name='inputField' variant="outlined" />
                <Button onSubmit={(e) => handleSubmit(e)} type="submit" variant="contained" sx={{ mt: 1, ml: 2 }}> Add </Button>

            </form>
        </Box>
    )
}

export default Form