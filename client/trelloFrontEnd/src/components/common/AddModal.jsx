import React from 'react';
import Form from './Form';

import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Modal from '@mui/material/Modal';
import AddIcon from '@mui/icons-material/Add';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,

};

export default function ModalForm({ onSubmitModal, label }) {

    const [open, setOpen] = React.useState(false);

    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);

    function handleOnChange(value) {
        
        if ((value.length > 0)) {

            onSubmitModal(value)
            setOpen(false)
        }
    }

    return (
        <div>
            <Button onClick={handleOpen} sx={{
                p: 1, mx: 4, color: 'black', borderColor: 'black', '&:hover': {
                    bgcolor: '#b4b3b5', borderColor: 'black'
                }
            }} endIcon={<AddIcon />} >Create {label} </Button>

            <Modal
                open={open}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style} >

                    <IconButton onClick={handleClose} sx={{ color: 'black', float: 'right', '&:hover': { color: "#b71c1c" } }}>
                        <CloseIcon />
                    </IconButton>
                    <Form onSubmitForm={handleOnChange} label={label} />

                </Box>

            </Modal>
        </div>
    );
}
