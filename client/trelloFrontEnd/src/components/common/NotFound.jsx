import React from 'react'
import Box from '@mui/material/Box';
import { Link } from 'react-router-dom';

export default function NotFound() {
  return (
    <>
      <Link to={`/`} style={{ textDecoration: 'none', position: 'relative', top: '5vh', left: '5vh' }} > <span style={{ border: '2px solid black', padding: '5px' }}>Back</span> </Link>
      <Box component="section" sx={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', m: 10, mt: 15, p: 2 }}>
        <h2>Not Found !!</h2>
        <h4>This page does not exist</h4>
      </Box>

    </>
  )
}

