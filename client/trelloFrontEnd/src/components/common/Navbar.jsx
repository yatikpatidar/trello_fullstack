import React from 'react'
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';

export default function Navbar() {
    return (

        <>
            <Box sx={{ px: 0 }}>

                <Box component="section" sx={{ m: 0, p: [2], backgroundColor: "gray", borderRadius: 2 }}>
                    <Typography variant="p" sx={{ px: 4, my: 0, fontSize: 28, color: 'white' }}>
                        Trello
                    </Typography>
                </Box>

            </Box>

        </>
    )
}

