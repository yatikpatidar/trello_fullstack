import React from 'react'
import { Link } from 'react-router-dom';

import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';

export default function DisplayBoard({ board }) {
    return (
        board && <>
            <Link to={`boards/${board['id']}`} style={{ textDecoration: 'none' }} >
                <Card sx={{ boxShadow: 3 }}>
                    <CardContent  >
                        <Typography gutterBottom variant="h5" component="div" sx={{ textDecoration: 'none' }} >
                            {board['board_name']}
                        </Typography>
                    </CardContent>
                </Card>
            </Link>
        </>
    )
}

