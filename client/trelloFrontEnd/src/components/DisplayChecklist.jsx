import React, { useState, useEffect } from 'react'
import Form from './common/Form';


import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import IconButton from '@mui/material/IconButton';
import DeleteIcon from '@mui/icons-material/Delete'
import Checkbox from '@mui/material/Checkbox';
import Button from '@mui/material/Button';
import Progress from './common/Progress';


import { trelloApi } from '../api/trelloApi';
import { useSelector, useDispatch } from 'react-redux'
import { setCheckItem, addCheckItem, deleteCheckItem ,updateCheckItem } from './../features/checkItemSlice'

export default function DisplayChecklist({ checklist, Item, onDeleteChecklist}) {

    const [showAddCheckItemForm, setShowAddCheckItemForm] = useState(false)
    const fetchUrl = `http://localhost:4000/checkItem/${checklist['id']}`
    const addUrl = `http://localhost:4000/checkItem/${checklist['id']}?name={name}`
    const deleteUrl = `http://localhost:4000/checkItem/{deleteId}`


    const [
        getData,
        postData,
        deleteData,
        putData
    ] = trelloApi(fetchUrl, addUrl, deleteUrl);

    const checkItemData = useSelector((state) => state.checkItem.checkItem)
    const dispatch = useDispatch()

    useEffect(() => {

        async function fetchCheckItem() {
            const jsonData = await getData()
            dispatch(setCheckItem({ 'jsonData': jsonData, 'checklistId': checklist['id'] }))
        }

        fetchCheckItem()

    }, [])

    function handleDeleteChecklist(e, id) {
        onDeleteChecklist(e, id);
    }

    async function handleCheckItemCheckbox(e, checkItemId, checklistId) {
        e.stopPropagation()
        const state = e.target.checked ? 'complete' : 'incomplete'
        const url = `http://localhost:4000/checkItem/${checkItemId}?state=${state}`

        const response = await putData(url)
        dispatch(updateCheckItem({checkItemId, checklistId , state}))
        
    }

    async function handleDeleteCheckItem(e, itemID, checklistId) {
        await deleteData(itemID , 'delete')
        dispatch(deleteCheckItem({'checkItemId':itemID , 'checklistId':checklist['id']}))
    }

    function handleToggleButton(e) {
        e.stopPropagation();
        setShowAddCheckItemForm(!showAddCheckItemForm)
    }
    async function handleAddCheckItem(value) {
        const response = await postData(value)
        dispatch(addCheckItem({ 'response': response, checklistId: checklist['id'] }))

    }

    return (

        <Box component="section" spacing={4} sx={{ mb: 2, p: 1, borderRadius: 2, border: '1px solid black', boxShadow: 4 }}>
            <Item sx={{ display: 'flex', justifyContent: 'space-between' }}><Typography gutterBottom variant="h6" component="div" sx={{ mt: 1, ml: 1, textDecoration: 'none' }} >
                {checklist['checklist_name']}
            </Typography>
                {checkItemData[checklist['id']] && checkItemData[checklist['id']].length > 0 && (
                    <Checkbox disabled checked />
                )}
                <Button onClick={(e) => handleToggleButton(e)} variant="" >Add CheckItem</Button>
                <IconButton onClick={(e) => handleDeleteChecklist(e, checklist['id'])} aria-label="delete" sx={{ '&:hover': { color: "#b71c1c" } }}>
                    <DeleteIcon fontSize="small" />
                </IconButton>

            </Item>
            <Box sx={{ m: 1 }} >

                <Box sx={{ display: 'flex', alignItems: 'center' }}>
                {checkItemData[checklist['id']] && <Progress checkItems={checkItemData[checklist['id']]} />}
                </Box>
                {checkItemData[checklist['id']] && checkItemData[checklist['id']].map((item, index) => {
                    return <Item key={`checkItemsKey${index}`} sx={{ display: 'flex', justifyContent: 'space-between' }}>
                        <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
                            <Checkbox onChange={(e) => handleCheckItemCheckbox(e, item['id'], checklist['id'])} checked={item.state == 'complete'} />
                            <Typography gutterBottom variant="h6" component="div" sx={{ textDecoration: 'none' }} >
                                {item['checkItem_name']}
                            </Typography>
                        </Box>
                        <IconButton onClick={(e) => handleDeleteCheckItem(e, item['id'], checklist['id'])} aria-label="delete" sx={{ '&:hover': { color: "#b71c1c" } }}>
                            <DeleteIcon fontSize="small" />
                        </IconButton>
                    </Item>
                })}
                {showAddCheckItemForm && (
                    <Form onSubmitForm={handleAddCheckItem} label={"CheckItem"} />
                )}

            </Box>
        </Box>
    )

}
