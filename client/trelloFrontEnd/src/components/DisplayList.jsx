import React, { useState, useEffect, useContext, useReducer } from 'react'
import DisplayCard from './DisplayCard';
import Form from './common/Form';

import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import IconButton from '@mui/material/IconButton';
import DeleteIcon from '@mui/icons-material/Delete';

import { trelloApi } from '../api/trelloApi';

import { useSelector, useDispatch } from 'react-redux'
import { setCards, addCard, deleteCard } from '../features/cardsSlice';

const Item = styled(Paper)(({ theme }) => ({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
}));

export default function DisplayList({ list, onDeleteList }) {

    const fetchUrl = `http://localhost:4000/card/${list['id']}`
    const addUrl = `http://localhost:4000/card/${list['id']}?name={name}`
    const deleteUrl = `http://localhost:4000/card/{deleteId}`

    const [
        getData,
        postData,
        deleteData
    ] = trelloApi(fetchUrl, addUrl, deleteUrl, false);

    const cardsData = useSelector((state) => state.cards.cardsData)
    const dispatch = useDispatch()

    useEffect(() => {

        async function fetchCardsData() {
            const jsonData = await getData()
            dispatch(setCards({ 'jsonData': jsonData, listId: list['id'] }))
        }

        fetchCardsData()

    }, [])

    function handleDeleteList(id) {
        onDeleteList(id)
    }

    async function handleAddCard(cardname) {
        const response = await postData(cardname)
        dispatch(addCard({ 'response': response, listId: list['id'] }))
    }

    async function handleDeleteCard(id) {
        await deleteData(id, 'delete')
        dispatch(deleteCard({ cardId: id, listId: list['id'] }))
    }


    return (
        <>
            <Box component="section" spacing={4} sx={{ m: 1, p: 2, minWidth: '25%', border: '2px solid black', borderRadius: 4, boxShadow: 2 }}>
                <Item sx={{ boxShadow: 3, display: 'flex', justifyContent: 'space-between' }}><Typography gutterBottom variant="h5" component="div" sx={{ textDecoration: 'none', ml: 2 }} >
                    {list['list_name']}
                </Typography>
                    <IconButton onClick={() => handleDeleteList(list['id'])} aria-label="delete" sx={{ '&:hover': { color: "#b71c1c" } }}>
                        <DeleteIcon />
                    </IconButton>
                </Item>

                <Box component="section" sx={{ display: 'column', p: 2, boxShadow: 3 }}>
                    {cardsData[list['id']] && cardsData[list['id']].map((card, index) => {
                        return <DisplayCard key={`card${index}`} card={card} Item={Item} onDeleteCard={handleDeleteCard} />
                    })}
                </Box>

                <Form onSubmitForm={handleAddCard} label={"Card"} />

            </Box>
        </>
    )
}
